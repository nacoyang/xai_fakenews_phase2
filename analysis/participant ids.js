
// Type 1: down-down 
// Type 2: down-up
// Type 3: up-up
// Type 4: up-down
// Type 5: same-same


participantObjs_noai = [
reqi8LXz,
fjU8dWcr,
hd6p2u8e,
yFENm2xt,
CUd69GqQ,
QxvW7OEq,
F344S74r,
M5ZimiR_,
r14kK3yE,
vISPNnD2,
Xs1yVbdM,
ZD96aNsu,
t1Ri6YcJ,
vNye1enW,
jUwkfvb7,
u3k1esnR,
S20AMxAM,
AG-xrB5m,
YVD5rl8N,
kZohR6JR,
WjoxLFJL,
QtCImrhZ,
KZOoEbkz,
b74rxTzQ,
OJHBVbVK,
1n7rNByh,
eIEx6Ja7,
Lt5JNaAU,
nBBzC5QY,
_Wnmdb-8,
nAvY0Q3C,
WTXlRnWK,
NmLyf4op,
ol0dsM-N,
yRLxtkuY];


participantObjs_xai_attr = [
xz_nxk0p,
CJVVM0wI,
0GFdomjq,
5weEGC-n,
0Ta0hsxH,
PjLUcK3g,
HYe8x1C0,
m88Xt8q-,
u-RgBpD1,
KnCDH5a4,
Cs_YmyI3,
HHACiFo7,
LRXXdn_X,
C8kggzb9,
x7YpQKN4,
0MsIp6Lb,
KecW-HKq,
BVU8KWBt,
I2Miv5VX,
LW1yhblP,
ALKG3eOZ,
1SGV3hLZ,
iN5Ct-1w,
X9DrP0hQ,
NOps0P6h,
X_qqEUTH,
Z-eiSo68,
ibODpQlh,
eWLLCBjp,
WJttQeQu,
PZy4bLab,
K88IqjC0,
IakQxcn4,
4-RDr1Ou,
R5ug8x2J,
xytPGZcN,
5mZ76qYF,
41i7QoJm,
AFocSGYP];

// w/out AI check
error_ai { T1: 34, T2: 21, T3: 27 }
error_human { T1: 66, T2: 50, T3: 58 }
error_human_ai { T1: 67, T2: 36, T3: 67 }

// with AI check
error_ai { T1: 28, T2: 14, T3: 12 }
error_human { T1: 56, T2: 38, T3: 46 }
error_human_ai { T1: 43, T2: 17, T3: 28 }



participantObjs_xai_attn = [
m5j3S1nU, 
xTX1asdV,
sJdGiRDe,
32_8sHbo,
yM7jTXz0,
ga3sZ53C,
TWw-ixzM,
XPEDF5uL,
B652jZI2,
PuUDb_PS,
GocJYNFa,
hSECKshh,
h0mVFMto,
0tIVfJ0l,
WhNOnytz,
uv54Qr9M,
CFaaJP_h,
2cPZqJ-2,
DnPYZmmr,
SF6qj2s7,
mBLUBFQc,
phb3XUqB,
8m6-D093,
ftVbRmrm,
Guc8UpWN,
68oIWMGY,
YBRrG8VE,
BtnJ52yh,
5kNAO9jC,
xTmriPWi,
Qf8Wl9nU,
v4cYYjwU,
z4Yipaci,
X4ReMPzv,
Xjynhs6w,
xAJs5ubp,
lJnHiTII,
xcPdKgq9,
IhFdmY_s,
TcbiRSol,
cieYxCjP];

// w/out AI check
error_ai { T1: 63, T2: 25, T3: 41 }
error_human { T1: 44, T2: 25, T3: 27 }
error_human_ai { T1: 94, T2: 42, T3: 105 }

// with AI check
error_ai { T1: 51, T2: 13, T3: 21 }
error_human { T1: 24, T2: 12, T3: 14 }
error_human_ai { T1: 62, T2: 25, T3: 58 }



participantObjs_ai = [
VRJ_9Niy,
aqDHSut2,
c9YPPGvO,
ibi44DW6,
GxfNSM7n,
8amGyE8I,
4q3hLAqQ,
2zqM-Td-,
6IYp7tUr,
aOh2KTdJ,
6m1EmXW8,
aaFdA9bz,
FM28LToE,
EESlPfBS,
lSKm99F-,
-Ec-vlIE,
w8xDjHlV,
7lDD1BMh,
W0bSHI6l,
PeyA-C1-,
PfrYFJo2,
7CMI9bJu,
ufXLtTVX,
Wvrmixfc,
Xx22iduT,
se59Tzuh,
QyxIsh32,
ntEwhdrr,
axmkA8h3,
ndcNJ9Ft,
huEqtDzr,
ircADUv5,
b-dDvAb7,
ZTTupLtM,
dtWYh9gW,
VqxQDfoU,
H7RLHzMP,
xbvLgpuR];


// w/out AI check
error_ai { T1: 48, T2: 24, T3: 36 }
error_human { T1: 56, T2: 46, T3: 44 }
error_human_ai { T1: 68, T2: 35, T3: 61 }

// with AI check
error_ai { T1: 39, T2: 14, T3: 22 }
error_human { T1: 38, T2: 33, T3: 34 }
error_human_ai { T1: 47, T2: 19, T3: 34 }



participantObjs_xai_all = [
YgC3Lr_T,
uBrbsMqC,
-fHqL3im,
WiQ2iyHw,
XFnH8qhdC,
5fQRtGN1,
V7NGxTqv,
V9yO3nhK,
irVq3fuc,
fOJ433JE,
fssvqbNB,
igof6hGu,
g22KQnuF,
Q1D7iyGq,
5EeS0Psd,
H99gBox9,
_vWR6kcy,
ktyNRR6ZG,
sPtlGW4I,
Wyxx4SFK,
_JqVJAxw,
Ov5CXHbP,
ZAHmI4Ho,
UlIXz_qn,
EACjte44,
2he7JPq1,
8LLPQyo0,
4QfoOtXS,
JUQGh40M,
DUHtLHzo,
0oIU3YEt,
Ya1dFNYM,
Z9f_NWAK,
l82YP12i,
qBFHbbw9,
7uuX5q3O,
fir8zZkY,
bfojgREP,
hhwmA7Re];


// w/out AI check
error_ai { T1: 42, T2: 23, T3: 38 }
error_human { T1: 57, T2: 41, T3: 43 }
error_human_ai { T1: 46, T2: 21, T3: 66 }

// with AI check
error_ai { T1: 32, T2: 18, T3: 23}
error_human { T1: 44, T2: 35, T3: 40}
error_human_ai { T1: 30, T2: 14, T3: 43}



//  New data: 


// --- removed  --> 
// no-ai: doesn't work: 
//      time outlier: [a5YMOblZ,8eXULRXM]
//      engagement outlier: [jXd-eRok,_Wnmdb-8]
//      study error: []


// ai: []  doesn't work: []   
    //      time outlier: []    
    //      engagement outlier: [7gP786YS,dDOIvVqs] << google
    //      study error: []
    
// xai-attn: does not wiork: []
    //       time outlier: [XPEDF5uL,wvGfdVHd,Jtc_Bl5c]
    //       engagement outlier: [ozFUG1mL]
    //       study error: [FqN3gjUG,lco_gcxs]   


// xai-attr:  doesn't work: []
    //      time outlier: [0GFdomjq,htZ2_cjo,2V8ShdB7]
    //      engagement outlier: []
    //      study error: [AnwD8aCJ,GMWkgx8lt]   

// XAI-all: doesn't work: []
    //      time outlier: [nqSn0N5p]
    //      engagement outlier: 
    //      study error: [pq8HkY85]   




/// ----------- Performance Types -----------------



// down-down: 19 
Type_1 = [
uBrbsMqC,
fssvqbNB,
Q1D7iyGq,
UlIXz_qn,
4QfoOtXS,
fir8zZkY,
C8kggzb9,
Z-eiSo68,
ibODpQlh,
5mZ76qYF,
v4cYYjwU,
lJnHiTII,
cieYxCjP,
6IYp7tUr,
6m1EmXW8,
FM28LToE,
7CMI9bJu,
se59Tzuh,
ZTTupLtM]


// w/out AI check
error_ai { T1: 26, T2: 12, T3: 10 }
error_human { T1: 22, T2: 26, T3: 21 }
error_human_ai { T1: 14, T2: 14, T3: 37 }

// with AI check
error_ai { T1: 25, T2: 8, T3: 4 }
error_human { T1: 16, T2: 21, T3: 17 }
error_human_ai { T1: 10, T2: 8, T3: 18 }



// down-up 26
Type_2 = [
YgC3Lr_T,
V7NGxTqv,
irVq3fuc,
fOJ433JE,
8LLPQyo0,
JUQGh40M,
xz_nxk0p,
CJVVM0wI,
BVU8KWBt,
I2Miv5VX,
iN5Ct-1w,
NOps0P6h,
GocJYNFa,
uv54Qr9M,
68oIWMGY,
YBRrG8VE,
X4ReMPzv,
IhFdmY_s,
TcbiRSol,
ibi44DW6,
4q3hLAqQ,
aaFdA9bz,
7lDD1BMh,
Wvrmixfc,
dtWYh9gW,
H7RLHzMP]

// w/out AI check
error_ai { T1: 33, T2: 9, T3: 32 }
error_human { T1: 32, T2: 36, T3: 26 }
error_human_ai { T1: 33, T2: 48, T3: 34 }

// with AI check
error_ai { T1: 30, T2: 6, T3: 21 }
error_human { T1: 25, T2: 26, T3: 19 }
error_human_ai { T1: 23, T2: 27, T3: 21 }



// up-up: 23 
Type_3 = [
WiQ2iyHw,
5fQRtGN1,
g22KQnuF,
sPtlGW4I,
Wyxx4SFK,
_JqVJAxw,
Ov5CXHbP,
0GFdomjq,
5weEGC-n,
u-RgBpD1,
HHACiFo7,
1SGV3hLZ,
eWLLCBjp,
PZy4bLab,
R5ug8x2J,
yM7jTXz0,
B652jZI2,
xTmriPWi,
c9YPPGvO,
8amGyE8I,
-Ec-vlIE,
ntEwhdrr,
huEqtDzr]

// w/out AI check
error_ai { T1: 24, T2: 15, T3: 24 }
error_human { T1: 41, T2: 26, T3: 18 }
error_human_ai { T1: 44, T2: 29, T3: 22 }


// with AI check
error_ai { T1: 17, T2: 10, T3: 13 }
error_human { T1: 28, T2: 18, T3: 14 }
error_human_ai { T1: 30, T2: 21, T3: 11 }


// up-down: 87
Type_4 = [
-fHqL3im,
XFnH8qhdC,
V9yO3nhK,
igof6hGu,
5EeS0Psd,
H99gBox9,
_vWR6kcy,
ktyNRR6ZG,
ZAHmI4Ho,
EACjte44,
2he7JPq1,
DUHtLHzo,
0oIU3YEt,
Ya1dFNYM,
Z9f_NWAK,
qBFHbbw9,
7uuX5q3O,
bfojgREP,
hhwmA7Re,
0Ta0hsxH,
PjLUcK3g,
HYe8x1C0,
m88Xt8q-,
KnCDH5a4,
Cs_YmyI3,
LRXXdn_X,
x7YpQKN4,
0MsIp6Lb,
KecW-HKq,
LW1yhblP,
ALKG3eOZ,
X9DrP0hQ,
X_qqEUTH,
WJttQeQu,
K88IqjC0,
IakQxcn4,
4-RDr1Ou,
xytPGZcN,
41i7QoJm,
AFocSGYP,
m5j3S1nU,
xTX1asdV,
sJdGiRDe,
32_8sHbo,
ga3sZ53C,
TWw-ixzM,
XPEDF5uL,
PuUDb_PS,
hSECKshh,
h0mVFMto,
0tIVfJ0l,
WhNOnytz,
CFaaJP_h,
2cPZqJ-2,
DnPYZmmr,
SF6qj2s7,
mBLUBFQc,
phb3XUqB,
8m6-D093,
ftVbRmrm,
Guc8UpWN,
BtnJ52yh,
5kNAO9jC,
Qf8Wl9nU,
z4Yipaci,
Xjynhs6w,
xAJs5ubp,
xcPdKgq9,
VRJ_9Niy,
aqDHSut2,
GxfNSM7n,
2zqM-Td-,
aOh2KTdJ,
EESlPfBS,
lSKm99F-,
w8xDjHlV,
W0bSHI6l,
PeyA-C1-,
PfrYFJo2,
ufXLtTVX,
Xx22iduT,
QyxIsh32,
axmkA8h3,
ndcNJ9Ft,
ircADUv5,
b-dDvAb7,
VqxQDfoU,
xbvLgpuR]


// --performance--
// 1 down-down: 19 | 12.2%
// 2 down-up 26    | 16.7%
// 3 up-up: 23     | 14.8%
// 4 up-down: 87   | 56.1%
// total: 155


// --reliance--
// 1 down-down: 33 | 21.1%
// 2 down-up    29 | 18.6%
// 3 up-up:     26 | 16.6%
// 4 up-down:   66 | 42.3%
// total: 156 



% ANOVA: F(df_between,df_within) = F_ration, p = p_value
$\chi^2(12, N=187)=21.884$ and $p = 0.039$


// 1- Use a new measure for user engagement with XAI --> (prediction + explanation engagement)
// 2- Describe performance drop is not because of Reliance on AI:
            >> maybe user skip over time ? 
            >> maybe human-ai error over lap over time ?
            >> maybe unlearning things ?


// w/out AI check
error_ai { T1: 102, T2: 55, T3: 74 }
error_human { T1: 127, T2: 74, T3: 107 }
error_human_ai { T1: 184, T2: 42, T3: 205 }


// with AI check
error_ai { T1: 76, T2: 33, T3: 39 }
error_human { T1: 93, T2: 53, T3: 84 }
error_human_ai { T1: 119, T2: 18, T3: 112 }




// User reliance type: 2 down-up
error_ai { T1: 29, T2: 17, T3: 14 }
error_human { T1: 27, T2: 9, T3: 23 }
error_human_ai { T1: 28, T2: 37, T3: 22 }



// User reliance type: 3 up-up
error_ai { T1: 18, T2: 6, T3: 4 }
error_human { T1: 25, T2: 22, T3: 40 }
error_human_ai { T1: 40, T2: 10, T3: 7 }



// User reliance type: 4 up-down
error_ai { T1: 79, T2: 20, T3: 42 }
error_human { T1: 66, T2: 63, T3: 50 }
error_human_ai { T1: 92, T2: 16, T3: 96 }



